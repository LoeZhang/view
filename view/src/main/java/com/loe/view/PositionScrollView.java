package com.loe.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class PositionScrollView extends ScrollView
{
    public PositionScrollView(Context context)
    {
        super(context);
    }

    public PositionScrollView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public PositionScrollView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    private OnScrollListener onScrollListener = null;

    public void setOnScrollListener(OnScrollListener onScrollListener)
    {
        this.onScrollListener = onScrollListener;
    }

    @Override
    protected void onScrollChanged(int x, int y, int oldx, int oldy)
    {
        super.onScrollChanged(x, y, oldx, oldy);
        if (onScrollListener != null)
        {
            onScrollListener.onScrollChanged(y);
        }
    }

    public interface OnScrollListener
    {
        void onScrollChanged(int y);
    }

    public int getScrollHeight()
    {
        return getChildAt(0).getHeight() - getHeight();
    }
}
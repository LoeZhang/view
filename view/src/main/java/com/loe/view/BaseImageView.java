package com.loe.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

@SuppressLint("AppCompatCustomView")
public class BaseImageView extends ImageView
{
    private String urlString = "";

    public BaseImageView(Context context)
    {
        super(context);
    }

    public BaseImageView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public BaseImageView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public String getUrlString()
    {
        return urlString;
    }

    public void setUrlString(String urlString)
    {
        this.urlString = urlString;
    }
}

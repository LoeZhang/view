package com.loe.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

@SuppressLint("AppCompatCustomView")
public class CheckTextView extends TextView
{
    public CheckTextView(Context context)
    {
        super(context);
    }

    public CheckTextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, context.obtainStyledAttributes(attrs, R.styleable.CheckTextView));
    }

    public CheckTextView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, context.obtainStyledAttributes(attrs, R.styleable.CheckTextView, defStyleAttr, 0));
    }

    @SuppressLint("NewApi")
    public CheckTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, context.obtainStyledAttributes(attrs, R.styleable.CheckTextView, defStyleAttr, defStyleRes));
    }

    protected boolean isAutoVisible;

    private int top;
    private int width;
    private int height;
    private int padding;

    private int color0;
    private int color1;
    // non enable
    private int color2;

    private Drawable checkDrawable0;
    private Drawable checkDrawable1;
    private Drawable checkDrawable2;

    private boolean checked = false;

    private void init(Context context, TypedArray typedArray)
    {
        int def = dp_px(14);
        setAutoVisible(typedArray.getBoolean(R.styleable.CheckTextView_check_auto_visible, false));
        checked = typedArray.getBoolean(R.styleable.CheckTextView_check_isChecked, false);
        boolean clickable = typedArray.getBoolean(R.styleable.CheckTextView_check_clickable, true);
        top = (int) typedArray.getDimension(R.styleable.CheckTextView_check_top, dp_px(0.5f));
        width = (int) typedArray.getDimension(R.styleable.CheckTextView_check_width, def);
        height = (int) typedArray.getDimension(R.styleable.CheckTextView_check_height, def);
        padding = (int) typedArray.getDimension(R.styleable.CheckTextView_check_padding, dp_px(5));
        color0 = typedArray.getColor(R.styleable.CheckTextView_check_color0, 0);
        color1 = typedArray.getColor(R.styleable.CheckTextView_check_color1, getResources().getColor(R.color.colorPrimary));
        color2 = typedArray.getColor(R.styleable.CheckTextView_check_color2, getResources().getColor(R.color.grayColor));
        int checkImage0 = typedArray.getResourceId(R.styleable.CheckTextView_check_image0, R.mipmap.icon_check_0);
        int checkImage1 = typedArray.getResourceId(R.styleable.CheckTextView_check_image1, R.mipmap.icon_check_1);
        int checkImage2 = typedArray.getResourceId(R.styleable.CheckTextView_check_image2, R.mipmap.icon_check_2);

        checkDrawable0 = getResources().getDrawable(checkImage0);
        checkDrawable0.setBounds(0, top, width, height + top);
        if (color0 != 0)
        {
            checkDrawable0.setColorFilter(color0, PorterDuff.Mode.SRC_IN);
        }

        checkDrawable1 = getResources().getDrawable(checkImage1);
        checkDrawable1.setBounds(0, top, width, height + top);
        if (color1 != 0)
        {
            checkDrawable1.setColorFilter(color1, PorterDuff.Mode.SRC_IN);
        }

        checkDrawable2 = getResources().getDrawable(checkImage2);
        checkDrawable2.setBounds(0, top, width, height + top);
        if (color2 != 0)
        {
            checkDrawable2.setColorFilter(color2, PorterDuff.Mode.SRC_IN);
        }

        update();

        setCompoundDrawablePadding(padding);

        setClickable(clickable);
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        update();
    }

    private boolean isClickInit = false;

    @Override
    public void setClickable(boolean clickable)
    {
        super.setClickable(clickable);
        if (clickable && !isClickInit)
        {
            setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    toggle();
                }
            });
            isClickInit = true;
        }
    }

    public void setCheckImage0(@DrawableRes int resId, @ColorInt Integer color)
    {
        if (color == null)
        {
            color = 0;
        }
        color0 = color;

        checkDrawable0 = getResources().getDrawable(resId);
        checkDrawable0.setBounds(0, top, width, height + top);
        if (color0 != 0)
        {
            checkDrawable0.setColorFilter(color0, PorterDuff.Mode.SRC_IN);
        }
        update();
    }

    public void setCheckImage1(@DrawableRes int resId, @ColorInt Integer color)
    {
        if (color == null)
        {
            color = 0;
        }
        color1 = color;

        checkDrawable1 = getResources().getDrawable(resId);
        checkDrawable1.setBounds(0, top, width, height + top);
        if (color1 != 0)
        {
            checkDrawable1.setColorFilter(color1, PorterDuff.Mode.SRC_IN);
        }
        update();
    }

    public void setCheckImage2(@DrawableRes int resId, @ColorInt Integer color)
    {
        if (color == null)
        {
            color = 0;
        }
        color2 = color;

        checkDrawable2 = getResources().getDrawable(resId);
        checkDrawable2.setBounds(0, top, width, height + top);
        if (color2 != 0)
        {
            checkDrawable2.setColorFilter(color2, PorterDuff.Mode.SRC_IN);
        }
        update();
    }

    public boolean isAutoVisible()
    {
        return isAutoVisible;
    }

    public void setAutoVisible(boolean autoVisible)
    {
        isAutoVisible = autoVisible;
        if (isAutoVisible)
        {
            setVisibility(getText().toString().isEmpty() ? GONE : VISIBLE);
            addTextChangedListener(new TextWatcher()
            {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after)
                {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count)
                {
                }

                @Override
                public void afterTextChanged(Editable s)
                {
                    if (isAutoVisible)
                    {
                        setVisibility(s.toString().isEmpty() ? GONE : VISIBLE);
                    }
                }
            });
        }
    }

    public void toggle()
    {
        onChecked(!checked);
    }

    public boolean isChecked()
    {
        return checked;
    }

    public void setChecked(boolean checked)
    {
        onChecked(checked);
    }

    private SwitchButton.OnCheckedChangeListener onCheckedChangeListener;

    public void setOnCheckedChangeListener(SwitchButton.OnCheckedChangeListener listener)
    {
        this.onCheckedChangeListener = listener;
    }

    protected void onChecked(boolean checked)
    {
        this.checked = checked;
        if (onCheckedChangeListener != null)
        {
            onCheckedChangeListener.OnCheckedChanged(checked);
        }
        update();
    }

    private void update()
    {
        setCompoundDrawables(checked ? (isEnabled() ? checkDrawable1 : checkDrawable2) : checkDrawable0, null, null, null);
    }

    public int dp_px(float dpValue)
    {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}
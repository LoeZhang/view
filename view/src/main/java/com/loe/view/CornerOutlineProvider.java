package com.loe.view;

import android.content.res.Resources;
import android.graphics.Outline;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.view.ViewOutlineProvider;

/**
 * 圆角剪切
 */
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class CornerOutlineProvider extends ViewOutlineProvider
{
    private float mRadius;

    public CornerOutlineProvider(float radius)
    {
        this.mRadius = px(radius);
    }

    @Override
    public void getOutline(View view, Outline outline)
    {
        Rect rect = new Rect();
        view.getGlobalVisibleRect(rect);
        int leftMargin = 0;
        int topMargin = 0;
        Rect selfRect = new Rect(leftMargin, topMargin, rect.right - rect.left - leftMargin, rect.bottom - rect.top - topMargin);
        outline.setRoundRect(selfRect, mRadius);
    }

    private static int px(float dpValue)
    {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}
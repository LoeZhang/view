package com.loe.view.viewpager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

/**
 * ViewPager选项卡基类
 *
 * @author zls
 * @version 1.0
 * @time 2016-4-16下午3:33:04
 */
public abstract class FragTabItem extends TabItem
{
    public Bundle args;

    protected Fragment fragment;

    public FragTabItem(Fragment fragment)
    {
        this.fragment = fragment;
    }

    public FragTabItem addArg(String key, int value)
    {
        if (args == null)
        {
            args = new Bundle();
        }
        args.putInt(key, value);
        return this;
    }

    public FragTabItem addArg(String key, String value)
    {
        if (args == null)
        {
            args = new Bundle();
        }
        args.putString(key, value);
        return this;
    }

    public FragTabItem addArg(String key, boolean value)
    {
        if (args == null)
        {
            args = new Bundle();
        }
        args.putBoolean(key, value);
        return this;
    }

    public FragTabItem addArg(String key, double value)
    {
        if (args == null)
        {
            args = new Bundle();
        }
        args.putDouble(key, value);
        return this;
    }

    public Fragment getFragment()
    {
        return fragment;
    }
}
package com.loe.view.viewpager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import java.util.Arrays;
import java.util.List;

public class LoeSlideViewPager extends ViewPager implements OnClickListener
{
    private List<FragTabItem> items;
    private FragmentManager fragmentManager;
    private FragAdapter pagerAdapter;
    private View line;
    private LinearLayout.LayoutParams params;
    private int W;
    public int index = 0;
    private double wRate;
    private int delW;
    /**
     * 是否可滑动
     */
    private boolean canMove = true;

    public LoeSlideViewPager(Context context)
    {
        super(context);
    }

    public LoeSlideViewPager(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public void init(FragmentManager fragmentManager, FragTabItem... items)
    {
        init(fragmentManager, null, 1, items);
    }

    public void init(FragmentManager fragmentManager, final View line, FragTabItem... items)
    {
        init(fragmentManager, line, 1, items);
    }

    public void init(FragmentManager fragmentManager, final View line, final double rate, FragTabItem... items)
    {
        this.items = Arrays.asList(items);
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null && fragments.size() == items.length)
        {
            for (int i = 0; i < fragments.size(); i++)
            {
                this.items.get(i).fragment = fragments.get(i);
            }
        }
        this.fragmentManager = fragmentManager;
        this.wRate = rate;
        // 初始化布局元素
        initItems();
        // 初始化ViewPager
        initPager();
        // 初始化item宽度
        if(line != null)
        {
            this.line = line;
            params = (LinearLayout.LayoutParams) line.getLayoutParams();
            line.post(new Runnable()
            {
                @Override
                public void run()
                {
                    W = line.getWidth() / LoeSlideViewPager.this.items.size();
                    delW = (int) (W * (1 - wRate) / 2 + 0.5);
                    params.width = (int) (W * wRate);
                    line.setLayoutParams(params);
                }
            });
        }

        onAttachedToWindow();
    }

    /**
     * 防止后台清理导致的下标不对应
     */
    @Override
    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        if (line != null)
        {
            line.post(new Runnable()
            {
                @Override
                public void run()
                {
                    setPosition(index * W);
                }
            });
        }
    }

    /**
     * 初始化item元素事件属性
     */
    private void initItems()
    {
        for (int i = 0; i < items.size(); i++)
        {
            items.get(i).view.setOnClickListener(this);
        }
    }

    /**
     * 初始化ViewPager及监听事件
     */
    private void initPager()
    {
        pagerAdapter = new FragAdapter(fragmentManager, items);
        setAdapter(pagerAdapter);
        // 设置切换效果
        //setPageTransformer(true, new RotatePageTransformer());
        // 设置缓存视图个数
        setOffscreenPageLimit(items.size() - 1);

        addOnPageChangeListener(new OnPageChangeListener()
        {
            @Override
            public void onPageSelected(int index)
            {
                selectItem(index);
                LoeSlideViewPager.this.index = index;
            }

            @Override
            public void onPageScrolled(int i, float scale, int arg2)
            {
                ////////////////////////////// 毛毛虫模式 ///////////////////////////////
                if(scale > 0.5)
                {
                    if(W != 0 ) params.width = (int) (W * wRate + W * (1 - scale) * 2);
                    scale = (scale - 0.5f) * 2;
                }else
                {
                    if(W != 0 ) params.width = (int) (W * wRate + W * scale * 2);
                    scale = 0;
                }
                ////////////////////////////////////////////////////////////////////////
                setPosition((int) ((i + scale) * W));
            }

            @Override
            public void onPageScrollStateChanged(int arg0)
            {
            }
        });
        // 第一次启动时选中第0个Fragment
        selectItem(0);
    }

    public void addOnPageSelectedListener(final IndexCallBack callBack)
    {
        addOnPageChangeListener(new OnPageChangeListener()
        {
            @Override
            public void onPageSelected(int index)
            {
                callBack.logic(index);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2)
            {
            }

            @Override
            public void onPageScrollStateChanged(int arg0)
            {
            }
        });
    }

    /**
     * 根据view设置选中的Fragment页
     */
    @Override
    public void onClick(View view)
    {
        for (int i  = 0; i< items.size(); i++)
        {
            FragTabItem fitem = items.get(i);
            if (view.equals(fitem.view))
            {
                // 更新选项卡
                selectItem(i);
                // 更新页面
                setCurrentItem(i);
            }
        }
    }

    /**
     * 设置滑动位置
     */
    private void setPosition(int position)
    {
        if(line != null)
        {
            params.setMargins(position + delW, 0, 0, 0);
            line.setLayoutParams(params);
        }
    }

    /**
     * 选择页面
     */
    public void select(int i)
    {
        // 更新选项卡
        selectItem(i);
        // 更新页面
        setCurrentItem(i);
    }

    public FragTabItem get(int index)
    {
        return items.get(index);
    }

    /**
     * 建立单个选中状态
     */
    private void selectItem(int index)
    {
        // 清除所有选择状态
        for (FragTabItem item : items)
        {
            item.onSelect(false);
        }
        // 添加单个选择状态
        items.get(index).onSelect(true);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev)
    {
        if (canMove)
        {
            return super.onInterceptTouchEvent(ev);
        }
        else
        {
            return false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev)
    {
        if (canMove)
        {
            return super.onTouchEvent(ev);
        }
        else
        {
            return false;
        }
    }

    public Fragment getNowFragment()
    {
        return get(index).fragment;
    }

    /**
     * 设置是否可滑动
     */
    public void setCanMove(boolean canMove)
    {
        this.canMove = canMove;
    }
}

package com.loe.view.viewpager;

import android.view.View;

public abstract class TabItem
{
    protected View view;

    abstract void onSelect(boolean isSelect);

    public View getView()
    {
        return view;
    }
}

package com.loe.view.viewpager;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

public class FragTextItem extends FragTabItem
{
    private TextView textView;

    private int unSelectColor;
    private int selectColor;

    public FragTextItem(Fragment fragment, TextView textView, int color0, int color1)
    {
        super(fragment);
        view = textView;
        this.textView = textView;

        unSelectColor = color0;
        selectColor = color1;
    }

    @Override
    public void onSelect(boolean isSelect)
    {
        if(isSelect)
        {
            textView.setTextColor(selectColor);
        }else
        {
            textView.setTextColor(unSelectColor);
        }
    }
}

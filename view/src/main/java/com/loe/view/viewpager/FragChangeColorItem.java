package com.loe.view.viewpager;

import android.graphics.PorterDuff;
import android.support.v4.app.Fragment;;

import com.loe.view.ImageTextView;

@SuppressWarnings("rawtypes")
public class FragChangeColorItem extends FragTabItem
{
    private ImageTextView textView;

    private int unSelectColor;
    private int selectColor;

    public FragChangeColorItem(Fragment fragment, ImageTextView textView, int color0, int color1)
    {
        super(fragment);

        this.view = textView;
        this.textView = textView;

        unSelectColor = color0;
        selectColor = color1;
    }

    @Override
    public void onSelect(boolean isSelect)
    {
        if(isSelect)
        {
            textView.setTextColor(selectColor);
            textView.getCompoundDrawables()[1].mutate().setColorFilter(selectColor, PorterDuff.Mode.SRC_ATOP);
        }else
        {
            textView.setTextColor(unSelectColor);
            textView.getCompoundDrawables()[1].mutate().setColorFilter(unSelectColor, PorterDuff.Mode.SRC_ATOP);
        }
    }
}

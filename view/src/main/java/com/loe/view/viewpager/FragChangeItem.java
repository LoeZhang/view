package com.loe.view.viewpager;

import android.support.v4.app.Fragment;

import com.loe.view.ImageTextView;

@SuppressWarnings("rawtypes")
public class FragChangeItem extends FragTabItem
{
    private ImageTextView textView;

    private int res0,res1;
    private int unSelectColor;
    private int selectColor;

    public FragChangeItem(Fragment fragment, ImageTextView textView, int res0, int res1, int color0,
                          int color1)
    {
        super(fragment);
        this.view = textView;
        this.textView = textView;

        this.res0 = res0;
        this.res1 = res1;
        unSelectColor = color0;
        selectColor = color1;
    }

    @Override
    public void onSelect(boolean isSelect)
    {
        if(isSelect)
        {
            textView.setTextColor(selectColor);
            textView.setTopImage(res1);
        }else
        {
            textView.setTextColor(unSelectColor);
            textView.setTopImage(res0);
        }
    }
}

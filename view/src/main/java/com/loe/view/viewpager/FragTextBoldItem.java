package com.loe.view.viewpager;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.widget.TextView;

@SuppressWarnings("rawtypes")
public class FragTextBoldItem extends FragTabItem
{
    private TextView textView;

    private int unSelectColor;
    private int selectColor;

    public FragTextBoldItem(Fragment fragment, TextView textView, int color0, int color1)
    {
        super(fragment);
        this.view = textView;
        this.textView = textView;

        unSelectColor = color0;
        selectColor = color1;
    }

    @Override
    public void onSelect(boolean isSelect)
    {
        if(isSelect)
        {
            textView.setTextColor(selectColor);
            textView.setTypeface(Typeface.DEFAULT_BOLD);
        }else
        {
            textView.setTextColor(unSelectColor);
            textView.setTypeface(Typeface.DEFAULT);
        }
    }
}

package com.loe.view;

import android.content.Context;
import android.util.AttributeSet;

public class AlphaImageView extends BaseImageView
{
    public AlphaImageView(Context context)
    {
        super(context);
        init(context);
    }

    public AlphaImageView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context);
    }

    public AlphaImageView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void  init(Context context)
    {
        setClickable(true);
    }

    @Override
    protected void dispatchSetPressed(boolean pressed)
    {
        setAlpha(pressed || !isEnabled() ? 0.65f : 1);
        super.dispatchSetPressed(pressed);
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        setAlpha(enabled ? 1 : 0.65f);
        super.setEnabled(enabled);
    }
}

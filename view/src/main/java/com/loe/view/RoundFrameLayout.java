package com.loe.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Outline;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.FrameLayout;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class RoundFrameLayout extends FrameLayout
{
    private int width, height;
    private float radius = 0;
    private int direction = 0;

    public RoundFrameLayout(Context context)
    {
        this(context, null);
    }

    public RoundFrameLayout(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
        init(context, attrs);
    }

    public RoundFrameLayout(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs)
    {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.RoundFrameLayout);
        radius = array.getDimension(R.styleable.RoundFrameLayout_radius, radius);
        direction = array.getInt(R.styleable.RoundFrameLayout_radius_direction, direction);

        array.recycle();

        isAlpha = isClickable();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
    {
        super.onLayout(changed, left, top, right, bottom);

        width = getWidth();
        height = getHeight();

        float l = 0;

        switch (direction)
        {
            case 1:
            case 3:
                if (width > height)
                {
                    l = Math.min(width / 2f, height);
                }
                else
                {
                    l = width / 2f;
                }
                break;
            case 2:
            case 4:
                if (height > width)
                {
                    l = Math.min(width, height / 2f);
                }
                else
                {
                    l = height / 2f;
                }
                break;
            default:
                l = Math.min(width, height) / 2f;
                break;
        }


        radius = Math.min(radius, l);

        //////////////////////////////////////////////////////////////////////////////
        setOutlineProvider(outlineProvider);
        setClipToOutline(true);
    }

    private ViewOutlineProvider outlineProvider = new ViewOutlineProvider()
    {
        @Override
        public void getOutline(View view, Outline outline)
        {
            switch (direction)
            {
                case 1:
                    outline.setRoundRect(0, 0, width, height + (int) radius, radius);
                    break;
                case 2:
                    outline.setRoundRect(-(int) radius, 0, width, height, radius);
                    break;
                case 3:
                    outline.setRoundRect(0, -(int) radius, width, height, radius);
                    break;
                case 4:
                    outline.setRoundRect(0, 0, width + (int) radius, height, radius);
                    break;
                default:
                    outline.setRoundRect(0, 0, width, height, radius);
                    break;
            }
        }
    };

    private boolean isAlpha = false;

    @Override
    protected void dispatchSetPressed(boolean pressed)
    {
        if(isAlpha) setAlpha(pressed || !isEnabled() ? 0.65f : 1);
        super.dispatchSetPressed(pressed);
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        if(isAlpha) setAlpha(enabled ? 1 : 0.65f);
        super.setEnabled(enabled);
    }
}

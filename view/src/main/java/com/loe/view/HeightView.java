package com.loe.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;

@SuppressLint("AppCompatCustomView")
public class HeightView extends View
{
    public HeightView(Context context, double height)
    {
        super(context);
        setMinimumHeight(dp_px(height));
    }

    public HeightView(Context context, int height)
    {
        super(context);
        setMinimumHeight(dp_px(height));
    }

    private static int dp_px(double dpValue)
    {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}
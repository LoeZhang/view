package com.loe.view.auto;

public interface AutoScaleLayout
{
    boolean isAutoScaleEnable();
}

package com.loe.test

import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.loe.view.DeleteMenuLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity()
{
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        letterView.setOnLetterChangedListener()
        { s, position ->
            Toast.makeText(this, s, Toast.LENGTH_SHORT).show()
        }

        imageTextView.setLeftImage(R.mipmap.loe_search_black, resources.getColor(R.color.colorPrimary))

        stateButton.setOnClickListener()
        {
            switchButton.setCheckedNoListener(!switchButton.isChecked)
            toggleImage.isSelect = !toggleImage.isSelect

            toggleImage.isToggleClickable = true
        }

        switchButton.setOnCheckedChangeListener()
        {
//            Toast.makeText(this, "$it", Toast.LENGTH_SHORT).show()
        }

//        checkTextView.setCheckImage1(R.mipmap.ic_launcher, null)

//        var g = GradientDrawable()
//        g.setColor(ContextCompat.getColor(this, R.color.colorPrimary))
//        g.cornerRadius = 12f
//        buttonTest.background = g

//        buttonTest.stateListAnimator = null


        deleteMenu.addView(
            View.inflate(this, R.layout.item_title, null),
            ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 100)
        )
        deleteMenu.addView(View.inflate(this, R.layout.item_title2, null),
            ViewGroup.MarginLayoutParams(200, ViewGroup.LayoutParams.MATCH_PARENT))


        deleteMenu.setLeftSwipe(false)
    }
}
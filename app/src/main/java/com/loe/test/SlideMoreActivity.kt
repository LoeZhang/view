package com.loe.test

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.loe.view.viewpager.FragTextItem
import com.loe.view.viewpager.FragTabItem
import kotlinx.android.synthetic.main.activity_slidemore.*

class SlideMoreActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_slidemore)

        val list = arrayListOf("全部", "时代大厦", "大多数", "阿大使馆", "框架还得花", "单独", "算多", "胜多负少", "的地方都", "方法", "分担分担")
        initViewPager(list)

//        imageButton.setOnClickListener()
//        {
//            imageButton.isEnabled = false
//        }

//        image.setOnClickListener()
//        {
//            Toast.makeText(this, "sdsd", Toast.LENGTH_SHORT).show()
//        }

//        titleView.setTitleVisible(true)
    }

    private fun initViewPager(list: ArrayList<String>)
    {
        val defaultColor = Color.GRAY
        val mainColor = Color.BLACK

        if (list.isNotEmpty())
        {
            val items = ArrayList<FragTabItem>()
            list.forEach()
            { title ->
                val titleView = layoutInflater.inflate(R.layout.item_title, null) as TextView
                titleView.text = title
                items.add(FragTextItem(PageFragment(), titleView, defaultColor, mainColor).addArg("title", title))
            }
            slideTabView.setItems(items)
//            slideTabView.viewPager = viewPager
            viewPager.init(supportFragmentManager, items)
        }
    }
}